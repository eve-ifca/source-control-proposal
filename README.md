# Table of Content
1. [Overview](#Overview)
1. [Current](#Current)
    1. [Technical Concerns](#Technical-Concerns)
    1. [Cultural Concerns](#Cultural-Concerns)
    1. [Design Proposal](#Design-Proposal)
    1. [Future Look Into](#Future-Look-Into)

## Overview
Proposal on how to manage development sources after moving to git.

## Current
### Technical Concerns
1. [ ] Existing branching strategy is not aligned with common git branching strategy.
1. [ ] Review on deployment strategy.
1. [ ] Current branching strategy brings complexity in CI/CD pipeline.
1. [ ] Design for scrum board handling in GitLab.

### Cultural Concerns
1. [ ] Learning curve on git concept.
1. [ ] Learning curve on daily git check-in process for developers.
1. [ ] Learning curve on merging tasks for person who holds merge responsibilities.

### Current Design Proposal
1. Separate code and db source control into 2 repo respectively.
1. Use git tag to tag versioning.

### Future Look Into
1. Fork repository.
1. Git submodules.
